package com.example.myapplication;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.transform.Result;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Link {
    @Multipart
    @POST("/api/v1/step")
    Call<ResponseBody> image(@Part MultipartBody.Part filePart, @Part Map<String,String> map);
}
